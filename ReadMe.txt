> dotnet new is4empty -n IdentityServer
> dotnet add IdentityServer.csproj package Microsoft.EntityFrameworkCore.SqlServer (use v2.1.4?)
- remove Sqlite Nuget package
- change all occurrences of Sqlite to SqlServer
- update connection strings
> dotnet run /seed
- create Users table
- comment out .AddTestUsers in Startup.csproj
- modify launchSettings.json according to requirement (copy from IdentityServer Combined template)

SQL Database
	- Clients table
		- Add client app esp its home URL
	- ClientRedirectUris
		- Add Angular app's redirect URL
	- ClientPostLogoutRedirectUris
		- Add Angular app's post logout redirect URL

Web API
	- Create the API project
	> dotnet new web -n HdApi
	- Add API project it to the solution
	> dotnet sln add HdApi\HdApi.csproj
	- Update launchSettings.json
	- Modify Startup.cs
	    public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();

            services.AddAuthentication("Bearer")
            .AddJwtBearer("Bearer", options =>
            {
                options.Authority = "http://localhost:5000";
                options.RequireHttpsMetadata = false;
                options.Audience = "hdapi";
            });

            services.AddCors(options =>
            {
                // this defines a CORS policy called "default"
                options.AddPolicy("default", policy =>
                {
                    policy.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
        }

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("default");
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc();
		}
		