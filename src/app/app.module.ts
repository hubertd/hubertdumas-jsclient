import { BrowserModule } from "@angular/platform-browser";
import { NgModule, APP_INITIALIZER } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import {
  AuthModule,
  OidcSecurityService,
  OpenIDImplicitFlowConfiguration,
  OidcConfigService,
  AuthWellKnownEndpoints
} from "angular-auth-oidc-client";
import { ReqInterceptorService } from "./services/req-interceptor.service";
import { MainComponent } from "./main/main.component";
import { UnauthorisedComponent } from "./unauthorised/unauthorised.component";

export function initializeApp(oidcConfigService: OidcConfigService) {
  return () => oidcConfigService.load_using_stsServer("http://localhost:5000");
}

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    UnauthorisedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule.forRoot(),
  ],
  providers: [
    OidcConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [OidcConfigService],
      multi: true
    },
    { provide: "Window", useValue: window },
    { provide: HTTP_INTERCEPTORS, useClass: ReqInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(
    private oidcConfigService: OidcConfigService,
    private oidcSecurityService: OidcSecurityService
  ) {

    this.oidcConfigService.onConfigurationLoaded.subscribe(() => {

      const originUrl = "http://localhost:4200";
      const authUrl = "http://localhost:5000";

      const config = new OpenIDImplicitFlowConfiguration();
      config.stsServer = authUrl;
      config.redirect_url = originUrl;
      config.client_id = "spa";
      config.scope = "openid profile hdapi";
      config.response_type = "id_token token";
      config.post_logout_redirect_uri = "http://localhost:4200/signout-callback-oidc";
      // config.start_checksession = true;
      config.silent_renew = true;
      config.silent_renew_url = "https://localhost:4200/silent-renew.html";


      const authWellKnownEndpoints = new AuthWellKnownEndpoints();
      authWellKnownEndpoints.setWellKnownEndpoints(this.oidcConfigService.wellKnownEndpoints);

      this.oidcSecurityService.setupModule(config, authWellKnownEndpoints);
    });

  }

}


