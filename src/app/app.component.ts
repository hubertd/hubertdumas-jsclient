import { Component, OnInit, OnDestroy } from "@angular/core";
import { OidcSecurityService } from "angular-auth-oidc-client";
import { filter, take } from "rxjs/operators";
import { Subscription } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, OnDestroy {
  isAuthorizedSubscription: Subscription;
  ids: any[] = [];

  constructor(
      public oidcSecurityService: OidcSecurityService,
      private http: HttpClient
    ) {
    this.oidcSecurityService.getIsModuleSetup().pipe(
      filter((isModuleSetup: boolean) => isModuleSetup), take(1))
      .subscribe((isModuleSetup: boolean) => {
        this.doCallbackLogicIfRequired();
      });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    // this.isAuthorizedSubscription.unsubscribe();
  }

  login() {
    this.oidcSecurityService.authorize();
  }

  logout() {
    this.oidcSecurityService.logoff();
  }

  sendRequest() {
    this.http.get("http://localhost:5001/api/identity")
    .subscribe((response: any[]) => {
      this.ids = response;
    });
  }

  private doCallbackLogicIfRequired() {
      if (window.location.hash) {
          this.oidcSecurityService.authorizedImplicitFlowCallback();
      }
  }
}
